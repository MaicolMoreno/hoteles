'use strict'

var    browserSync = require('browser-sync');
var    gulp = require('gulp');
var    sass = require('gulp-sass');


var htmlmin = require('gulp-htmlmin');
var imagemin = require('imagemin');
var del = require('del');
var usemin = require('gulp-usemin');
var uglify = require('gulp-uglify');
var htmlmin = require('gulp-htmlmin');
var cleanCss = require('gulp-clean-css');
var rev = require('gulp-rev');

sass.compiler = require('node-sass');
 
gulp.task('sass', function () {
  return gulp.src('./css/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
})
 
        gulp.task('sass:watch', function () {
        gulp.watch('./css/*.scss', ['sass']);
            }); 

    gulp.task('browser-sync', function () {
        var files = ['./*.html','./css/*.css','./images/*.{png,jpeg,jpg, gif}','./js/*.js']
        browserSync.init(files, {
            server: {
                baseDir:'./'
            }
        });
    });

        gulp.task('clean', function () {
            return gulp.src(['dist/styles', 'dist/scripts', 'dist/images'], { read: false }).pipe($.clean());
        });

        gulp.task('copyfonts', function(){
            gulp.src(['./node_modules/open-iconic/font/fonts/*.{ttf, woff, eof, svg, eot, olf, otf.*}'])
            .pipe(gulp.dest('./dist/fonts'));
        });


        gulp.task('imagemin', function () {
            return gulp.src('./images/*.{png, jpg, jpeg, gif}')
              .pipe(imagemin({optimitazionLevel: 3, progressive: true, intercaled:true}))
              .pipe(gulp.dest('dist/images'));
          });
   
          gulp.task('usemin', function() {
            return gulp.src('./*.html')
              .pipe(usemin({
                css: [ rev() ],
                html: [ htmlmin({ collapseWhitespace: true }) ],
                js: [ uglify(), rev() ],
                inlinejs: [ uglify() ],
                inlinecss: [ cleanCss(), 'concat' ]
              }))
              .pipe(gulp.dest('dist/'));
          });


          

            gulp.task('build', ['clean' ], function(){
                gulp.setMaxListeners('copyfonts','imagemin', 'usemin');
            })